/**
    Jaks -
 */
(function () {
  var root = this

  var StreamBase = require ('./StreamBase')
  var StreamModule = require ('./StreamModule')
  require ('./Utils')

  var XmlStream = function () {
    StreamBase.apply (this)

    var prv = {
      data : '',
      stack : [],
      current : null,
      top : null
    }

    this.__defineGetter__('mimetype', function () {
      return 'application/json'
    })

    this.__defineGetter__('result', function () {
      return prv.top
    })

    this.parseCDATA = function (node) {
      // Ignore for now
    }

    this.parseDECLARATION = function (node) {
      // Ignore for now
    }

    this.parseCOMMENT = function (node) {
      // Ignore for now
    }

    this.parseDOCTYPE = function (node) {
      // Ignore for now
    }

    this.parseELEMENT = function (node) {
      if (node.s.endswith('/>'))
        node.closed = true

      if (node.s[1] == '/') {
        node.name = node.s.substring(2, node.s.length - 1).trim()

        if (prv.current[0] != node.name)
          console.warn ('Wrong closing tag: ' + node.name);
        prv.stack.pop()
        prv.current =  prv.stack[prv.stack.length-1]
        return
      }

      node.n = node.s.substring (1, node.s.length - (node.closed ? 2 : 1))
      var k = node.n.indexOf (' ')
      node.name = (k > 0) ? node.n.substring (0, k) : node.n

      var newNode = [node.name]

      if (k > 0) {
        node.attr = node.n.substring (k)
        // PARSE ATTR
        var attributes = this.parseAttributes (node.attr)
        newNode.push (attributes)
      }

      // console.log (newNode, prv.stack)
      if (node.closed)
        prv.current.push (newNode)
      else {

        if (!prv.top) {
          prv.top = newNode
          prv.current = newNode
        } else {
          prv.current.push (newNode)
        }

        prv.current = newNode
        prv.stack.push (prv.current)
      }
    }

    this.parseTEXT = function (node) {
      if (prv.current && node.s.trim() != '')
        prv.current.push (node.s)
    }

    this.parseAttributes = function (string) {
      var attrs = {}
      var value = ''
      var variable = ''
      var scope

      for (var i=0; i < string.length; ++i) {
        if (scope == '=') {
          if (string[i] == '"') scope = '"'
          else if (string[i] == '\'') scope = '\''
          else console.log ('ERROR ' , string[i])
        } else if (scope == '\'' || scope == '"') {
          if (string[i] == scope) {
            attrs [variable] = value
            variable = ''
            value = ''
            scope = null
          } else
            value += string[i]

        } else if (string[i] == '=') {
          scope = '='
        } else if (string[i] != ' ') {
          variable += string[i]
        } else if (variable != '') {
          variable = ''
          value = ''
          scope = null
        }
      }
      return attrs;
    }

    this.write = function (txt) {

      var stBy = function (txt, sfx) {
        return txt.startswith (sfx)
      }

      var eTo = function (txt, pfx) {
        var k = 0
        while (!txt.substring(0,k).endswith (pfx) && txt.length > k) k++;
        return txt.length > k ? k : null
      }

      txt = prv.data + txt.toString()
      var l;
      for (; ; ) {
        if (stBy (txt, '<![CDATA['))  l = { k:eTo(txt, ']]>'), t:'CDATA' }
        else if (stBy (txt, '<?'))    l = { k:eTo(txt, '?>'), t:'DECLARATION' }
        else if (stBy (txt, '<!--'))  l = { k:eTo(txt, '-->'), t:'COMMENT' }
        else if (stBy (txt, '<!'))    l = { k:eTo(txt, '>'), t:'DOCTYPE' }
        else if (stBy (txt, '<'))     l = { k:eTo(txt, '>'), t:'ELEMENT' }
        else  l = { k:eTo(txt, '<')-1, t:'TEXT' }

        if (l.k > 0) {
          l.s = txt.substring (0, l.k)
          txt = txt.substring (l.k)
          this['parse'+l.t] (l);
        } else {
          prv.data = txt
          return
        }
      }
    }

    this.end = function () {
      this.step ('finish', [null, this.result])
    }
  }

  var XmlParser = StreamModule (XmlStream);
  XmlParser.XmlStream = XmlStream


  // Export the module --------------------
  if (typeof module !== 'undefined')
    module.exports = XmlParser
  else if (typeof exports !== 'undefined')
    exports = module.exports = XmlParser
  else
    root.XmlParser = XmlParser

}).call(this)

if (require.main === module) {

  var XmlParser = module.exports
  var fs = require ('fs')

  if (process.argv.length > 2) {
    url = process.argv[2];
    var fStream = fs.createReadStream(url)
    var xStream = new XmlParser.XmlStream()
    xStream.once ('finish', function () {
      console.log (JSON.stringify(xStream.result, null, 2))
    })
    fStream.pipe (xStream)
  }
}
