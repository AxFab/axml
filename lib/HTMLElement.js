/*
  This file is part of aXml.

    aXml is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    aXml is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with aXml.  If not, see <http://www.gnu.org/licenses/>.
*/
"use strict";

var HTMLNode = require ('./HTMLNode'),
    HTMLEventTarget = require ('./HTMLEventTarget'),
    Utils = require ('./Utils')

// ============================================================================
/// Abstract insterface for all DOM element
var HTMLElement = function (tag) 
{
  HTMLNode.apply (this, [tag])
  HTMLEventTarget.apply (this, [tag])

  // Private field ------------------------------------------------------------
  var prv = {
    accessKey: '', // FIXME Get/Set
    accessKeyLabel: '', // FIXME Get/Set
    classList: [],
    dir: 'auto',
    id: '',
    lang: '',
    title: ''
  }

  // Element properties -------------------------------------------------------
  /// get token list of class attribute
  this.__defineGetter__("classList", function() {
    return prv.classList.map (function(e) { return e });
  });

  /// Get the name(s) of the CSS class defined for this element as a astring
  this.__defineGetter__("className", function() {
    var str = ''
    for (var i=0; i<prv.classList.length; ++i) {
      if (str != '') str += ' ' 
      str += prv.classList[i]
    }
    return str;
  });

  /// Set the name(s) of the CSS class defined for this element split by a space
  this.__defineSetter__("className", function(val){
    prv.classList = val.split(' ');
  });


  // Get the text direction attribute for this element
  this.__defineGetter__("dir", function() {
    return prv.dir;
  });

  // Set the text direction attribute for this element
  this.__defineSetter__("dir", function(val) {
    if (val == 'auto' || val == 'ltr' || val == 'rtl')
      prv.dir;
    else 
      throw 'Bad attribute value for dir: ' + val
  });


  // Get the id attribute of the element
  this.__defineGetter__("id", function() {
    return prv.id;
  });

  // Set the id attribute of the element
  this.__defineSetter__("id", function(val) {
    prv.id = val ? val.toString() : null;
  });


  // Get the lang attribute of the element
  this.__defineGetter__("lang", function() {
    return prv.lang;
  });

  // Set the lang attribute of the element
  this.__defineSetter__("lang", function(val) {
    prv.lang = val ? val.toString() : null;
  });


  // Get the title attribute of the element
  this.__defineGetter__("title", function() {
    return prv.title;
  });

  // Set the title attribute of the element
  this.__defineSetter__("title", function(val) {
    prv.title = val ? val.toString() : null;
  });

  // Get the name of the tag for the given element.
  this.__defineGetter__("tagName", function(val) {
    return this.nodeName
  });

  // Element properties COMPLEX -----------------------------------------------
  this.__defineGetter__("innerHTML", function() {});
  this.__defineSetter__("innerHTML", function() {});
  this.__defineGetter__("outerHTML", function() {});
  this.__defineSetter__("outerHTML", function() {});


  this.HTMLElement = function () {
    return extend (this.HTMLNode(), this.HTMLEventTarget(), {
      className:this.className,
      dir:this.dir,
      id:this.id,
      lang:this.lang,
      title:this.title,
    })
  }

  this.init = function (tag) {

    if (!tag.attributes) return

    if (tag.attributes.class) this.className = tag.attributes.class
    if (tag.attributes.dir) this.dir = tag.attributes.dir
    if (tag.attributes.id) this.id = tag.attributes.id
    if (tag.attributes.lang) this.lang = tag.attributes.lang
    if (tag.attributes.title) this.title = tag.attributes.title

    if (tag.attributes.href) this.href = tag.attributes.href
  }

  this.toOpen = this.HTMLElement
  this.class = 'HTMLElement'
  this.init (tag);
}

if (module) module.exports = HTMLElement

/*
accessKey              GS
accessKeyLabel         GS
blur()                    F
attributes     
baseURI
childElementCount
childNodes
children
classList
className              GS
click()                   F
clientHeight
clientLeft
clientTop
clientWidth
contentEditable        GS
contextMenu            G
dataset                G
dir                    GS
draggable              GS
firstChild
firstElementChild
focus                     F
hidden                 GS
id
innerHTML
isContentEditable
itemId                 GS
itemProp               GS
itemRef      
itemScope
itemType
itemValue
lang                   GS
lastChild
lastElementChild
localName
namespaceURI
nextElementSibling
nextSibling
nodeName
nodeType
nodeValue
offsetHeight            G
offsetLeft              G
offsetParent            G
offsetTop               G
offsetWidth             G
onabort                 GS
onblur                  GS
oncanplay
oncanplaythrough
onchange
onclick
oncontextmenu
oncopy
oncut
ondblclick
ondrag
ondragend
ondragenter
...
onwheel                 GS
outerHTML
ownerDocument
parentElement
parentNode
prefix
previousElementSibling
previousSibling
properties              G
scrollHeight
scrollLeft
scrollLeftMax
scrollTop
scrollTopMax
scrollWidth
spellcheck             GS
style                  GS
tabIndex               GS
tagName
textContent
title                  GS
*/
