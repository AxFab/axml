/*
  This file is part of aXml.

    aXml is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    aXml is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with aXml.  If not, see <http://www.gnu.org/licenses/>.
*/
"use strict";

// ============================================================================
var HTMLEventTarget = function (tag) 
{
  // Private field ------------------------------------------------------------
  var prv = {
  }

  this.addEventListener = function (type, listener, useCapture, wantsUntrusted) {
    if (!prv.listeners[type])
      prv.listeners[type] = []
    prv.listeners[type].push ( { 
      listener: listener,
      useCapture: useCapture,
      wantsUntrusted: wantsUntrusted,
    })
  }

  this.dispatchEvent = function (type, opt) {
    if (!prv.listeners[type]) return
    for (var i=0; i<prv.listeners[type].length; ++i) {
      prv.listeners[type][i].listener (opt)
    }
  }

  this.removeEventListener = function (type, listener, useCapture) {
    if (!prv.listeners[type]) return
    for (var i=0; i<prv.listeners[type].length; ++i) {
      if (prv.listeners[type][i].listener == listener) {
        prv.listeners[type] = prv.listeners[type].slice(i)
        --i
      }
    }
  }

  this.HTMLEventTarget = function () {
    return {}
  }

  this.class = 'HTMLEventTarget'
}

if (module) module.exports = HTMLEventTarget

