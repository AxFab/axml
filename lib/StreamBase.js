

(function (){
  var root = this

  /** \class StreamBase
   */
  var StreamBase = function ()
  {
    // Private field ------------------------------------------------------------
    var prv = {
      events: {},
      steps: {},
    }

    this.on = function(event, callback) {
      if (!prv.events[event])
        prv.events[event] = []
      prv.events[event].push (callback)
    }

    this.trigger = function (event, opt) {
      if (!prv.events[event]) return
      for (var i=0; i<prv.events[event].length; ++i) {
        prv.events[event][i] (opt);
      }
    }

    this.once = function(step, callback) {
      if (!prv.steps[step])
        prv.steps[step] = { done:false, callbacks:[] }
      prv.steps[step].callbacks.push (callback)
      if (prv.steps[step].done)
        callback ()
    }

    this.step = function (step, params) {
      if (!prv.steps[step]) return
      prv.steps[step].done = true;
      for (var i=0; i<prv.steps[step].callbacks.length; ++i) {
        prv.steps[step].callbacks[i].apply(this, params);
      }
    }

    this.emit = function() {
      // console.log ('EMIT', arguments)
    }

    this.removeListener = function(type, listener) {
    }

    this.class = 'StreamBase'
  }

  // Export the module --------------------
  if (typeof module !== 'undefined')
    module.exports = StreamBase
  else if (typeof exports !== 'undefined')
    exports = module.exports = StreamBase
  else
    root.StreamBase = StreamBase
}).call(this)
