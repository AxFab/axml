
(function() {
  var root = this

  // REQUIRE
  var DateFormat = root.DateFormat
  if( typeof DateFormat === 'undefined' ) {
    if( typeof require !== 'undefined' ) {
      DateFormat = require('./DateFormat')
    }
    else throw new Error('Utils requires DateFormat');
  }
  // REQUIRE
  //var DateFormat = require ('./DateFormat')

  // ===========================================================================
  String.prototype.trim=function()
  {
    return this.replace(/^\s+|\s+$/g, '');
  }

  String.prototype.ltrim=function()
  {
    return this.replace(/^\s+/,'');
  }

  String.prototype.rtrim=function()
  {
    return this.replace(/\s+$/,'');
  }

  String.prototype.fulltrim=function()
  {
    return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');
  }

  String.prototype.startswith=function(str)
  {
    return this.substring(0, str.length) == str;
  }

  String.prototype.endswith=function(str)
  {
    return this.substring(this.length - str.length, this.length) == str;
  }

  // ===========================================================================
  Array.prototype.contains = function (item)
  {
    for (var i=0; i<this.length; ++i)
      if (this[i] == item)
        return true;
    return false;
  }

  Array.prototype.squeeze=function(it)
  {
    if (it == undefined)
      it = ''
    for (var i=this.length-1; i;i--) {
      if (this[i] === it)
        this.splice(i,1)
    }
  }


  // ===========================================================================
  Date.prototype.format = function (mask, utc)
  {
    return DateFormat(this, mask, utc);
  }


  // ===========================================================================
  // Object.prototype.extend = function(obj)
  // {
  //   for (var k in obj)
  //     this[k] = obj[k];
  // }


}).call(this);
