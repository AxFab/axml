/*
  This file is part of aXml.

    aXml is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    aXml is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with aXml.  If not, see <http://www.gnu.org/licenses/>.
*/
"use strict";

var XMLNodeType = require ('./XMLNodeType')

// ============================================================================
/// Abstract insterface for all DOM nodes
var XMLNode = function (tag)
{
  // Private field ------------------------------------------------------------
  var prv = {
    baseURI: '',
    childNodes: [],
    nodeName: '',
    nodeType: XMLNodeType.ELEMENT_NODE, // FIXME
    nodeValue: null,
    ownerDocument: null,
    parentNode:null,

    nindex: 0,
    state:{
      layout:false,
    },
    userdata:{}
  }

  // Node properties ----------------------------------------------------------
  /// Get the string representing the base URL of the document
  this.__defineGetter__("baseURI", function() {
    return prv.baseURI
  });


  /// Get an array containing all the children of this node.
  this.__defineGetter__("childNodes", function(){
    return prv.childNodes.map (function(e) { return e });
  });

  /// Get the first direct child node of this node, or null if the node has no child.
  this.__defineGetter__("firstChild", function(){
    return prv.childNodes[0];
  });

  /// Get the last direct child node of this node, or null if the node has no child.
  this.__defineGetter__("lastChild", function(){
    return prv.childNodes[prv.childNodes.length-1];
  });


  /// Get a string representing the local part of the qualified name of an element
  /// DEPRECATED
  this.__defineGetter__("localName", function(){
    return (prv.nodeName[0] == '#') ? prv.nodeName : prv.nodeName.toLowerCase();
  });


  /// get the next node in the tree, or null if there isn't such node.
  this.__defineGetter__("nextSibling", function(){
    return prv.parentNode._getChild(prv.nindex + 1);
  });


  /// Get a string representing the name of the node
  this.__defineGetter__("nodeName", function(){
    return prv.nodeName;
  });


  /// Get a string representing the name of the node
  this.__defineGetter__("nodeType", function(){
    return prv.nodeType;
  });


  /// Get the value corresponds to the text data contained in the object.
  this.__defineGetter__("nodeValue", function(){
    return prv.nodeValue;
  });

  /// Set the value corresponds to the text data contained in the object.
  this.__defineSetter__("nodeValue", function(val){
    prv.nodeValue = val ? val.toString() : null;
  });


  /// Get the value corresponds to the text data contained in the object.
  this.__defineGetter__("ownerDocument", function(){
    return prv.ownerDocument;
  });

  /// Get the parent of this node. If there is no such node this property returns null.
  this.__defineGetter__("parentNode", function(){
    return prv.parentNode;
  });

  /// Get the parent of this node if it's an element.
  this.__defineGetter__("parentElement", function(){
    return prv.parentNode.nodeType == XMLNodeType.ELEMENT_NODE ? prv.parentNode : null;
  });


  /// get the previous node in the tree, or null if there isn't such node.
  this.__defineGetter__("previousSibling", function(){
    return prv.parentNode._getChild(prv.nindex - 1);
  });


  /// get the textual content of an element and all its descendants.
  this.__defineGetter__("textContent", function(){
    // FIXME
  });


  // Properties of ParentNode -------------------------------------------------

  /// Get an array containing all objects of type Element that are children of this ParentNode.
  this.__defineGetter__("children", function(){
    return prv.childNodes.map (function(e) { if (e.nodeType == XMLNodeType.ELEMENT_NODE) return e });
  });

  /// Get the first child of this ParentNode, or null if there is none.
  this.__defineGetter__("firstElementChild", function(){
    var i = 0
    while (i < prv.childNodes.length && prv.childNodes[i].nodeType != XMLNodeType.ELEMENT_NODE) ++i
    if (i < prv.childNodes.length)
      return prv.childNodes[i];
    return null
  });

  /// Get the last child of this ParentNode, or null if there is none.
  this.__defineGetter__("lastElementChild", function(){
    var i = prv.childNodes.length -1
    while (i >= 0 && prv.childNodes[i].nodeType != XMLNodeType.ELEMENT_NODE) --i
    if (i >= 0)
      return prv.childNodes[i];
    return null
  });

  /// Get the amount of children that the object has.
  this.__defineGetter__("childElementCount", function(){
    var c = 0,
        i = prv.childNodes.length -1
    while (i >= 0) {
      if (prv.childNodes[i].nodeType == XMLNodeType.ELEMENT_NODE)
        ++c
      --i
    }

    return c
  });

  // Properties of ElementTraversal -------------------------------------------

  /// get the next element in the tree, or null if there isn't such node.
  this.__defineGetter__("nextElementSibling", function(){
    var i = 1
    for (;;++i) {
      var next = prv.parentNode._getChild(prv.nindex + i)
      if (!next) return null
      if (next.nodeType == XMLNodeType.ELEMENT_NODE) return next;
    }
  });

  /// get the previous element in the tree, or null if there isn't such node.
  this.__defineGetter__("previousElementSibling", function(){
    var i = 1
    for (;;++i) {
      var prev = prv.parentNode._getChild(prv.nindex - i)
      if (!prev) return null
      if (next.nodeType == XMLNodeType.ELEMENT_NODE) return prev;
    }
  });


  // Protected methods --------------------------------------------------------
  this._setParentInfo = function (parent, index) {
    prv.parentNode = parent;
    prv.nindex = index;
  }

  this._getChild = function (index) {
    return prv.childNodes[index];
  }

  // Public methods -----------------------------------------------------------
  /// Insert a Node as the last child node of this element.
  /// @param child {XMLNode} child is the node to append underneath element. Also returned.
  this.appendChild = function (child) {
    child._setParentInfo (this, prv.childNodes.length);
    prv.childNodes.push (child);
    prv.state.layout = false
    return child
  }

  /// Clone a Node, and optionally, all of its contents. By default, it clones the content of the node.
  /// @param deep {boolean} true if the children of the node should also be cloned, or false to clone only the specified node.
  /// @return The new node that will be a clone of node
  this.cloneNode = function (deep) {
    // FIXME
  }

  this.compareDocumentPosition = function () {}
  this.contains = function () {}
  this.getUserData = function (key) {
    return prv.userdata[key].data
  }

  this.hasAttributes = function () {}

  this.hasChildNodes = function () {
    return prv.childNodes.length > 0
  }

  this.insertBefore = function () {}

  this.isDefaultNamespace = function () {
    return true;
  }

  this.isEqualNode = function () {}
  this.lookupPrefix = function () {}
  this.lookupNamespaceURI = function () {}
  this.normalize = function () {}

  /// Removes a child node from the DOM. Returns removed node.
  this.removeChild = function (child) {

    for (var i=0; i<prv.childNodes.length; ++i) {
      if (prv.childNodes == child) {
        prv.childNodes.shift(i)
        child._setParentInfo(null, 0)
        prv.state.layout = false

        for (; i<prv.childNodes.length; ++i) {
          prv.childNodes[i]._setParentInfo(this, i);
        }

        return child;
      }
    }

    throw ''
  }

  ///
  this.replaceChild = function (newChild, oldChild) {

    for (var i=0; i<prv.childNodes.length; ++i) {
      if (prv.childNodes == oldChild) {
        prv.childNodes[i] = newChild
        oldChild._setParentInfo(null, 0)
        prv.state.layout = false
        return oldChild;
      }
    }

    throw ''
  }

  this.setUserData = function (key, data, handle) {
    prv.userdata[key] = { data:data, handle:handle }
  }




  // Personnal extentions -----------------------------------------------------
  this.toText = function () {
    var str = ''
    str += 'Type: ' + this.nodeType + ', Name: ' + this.nodeName

    return str
  }

  this.XMLNode = function () {
    var obj = {
      baseURI:this.baseURI,
      nodeName:this.nodeName,
      nodeType:this.nodeType,
      nodeValue:this.nodeValue,
    }

    if (prv.childNodes.length) {
      obj.childNodes = []
      for (var i=0; i<prv.childNodes.length; ++i) {
        obj.childNodes.push (prv.childNodes[i].toOpen())
      }
    }

    return obj
  }

  this.init = function (tag) {
    if (tag.name) {
      if (tag.name == '_') {
        prv.nodeName = '#text'
        prv.nodeType = XMLNodeType.TEXT_NODE
        prv.nodeValue = tag.value
      } else {
        prv.nodeName = tag.name.toUpperCase()
        prv.nodeType = XMLNodeType.ELEMENT_NODE

        // FIXME attributes
      }
    }
  }

  this.toOpen = this.XMLNode
  this.class = 'XMLNode'
  this.init (tag);
}

if (module) module.exports = XMLNode

