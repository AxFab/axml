

(function (){
  var root = this

  /** \fn StreamModule
   * Create a module from a stream
   */
  var StreamModule = function (streamClass) {

    var _Module = function (opt) {

      var _module = {}

      /** \fn createStream
       * Create a stream to read and
       */
      _module.createStream = function () {
        return new streamClass ()
      },

      /** \fn createStream
       * Convert raw data
       */
      _module.convert = function (data) {
        var stream = _module.createStream()
        stream.write (data)
        stream.end()
        return stream.result
      },

      /** \fn readFile
       * Read and parse a html template file
       */
      _module.readFile = function (url, callback) {
        var fs = require('fs')
        var fd = fs.createReadStream (url)
        var stream = _module.createStream()
        stream.once ('finish', function (err, oaxd) {
          if (callback)
            callback (err, oaxd)
        })
        stream.pipe (fd)
      },

      /** \fn request
       * Respond to a http request
       */
      _module.request = function (req, res, next) {
        Markdown.readFile (req.path, function(err, data) {
          switch (err) {
            case 'ENOENT':
              Utils.httpResponse (opt, res, 404, 'text/html', opt.err404);
              break;
            case null:
              Utils.httpResponse (opt, res, 200, streamClass.mimetype, data);
              break;
          }
        })
      }

      return _module
    }

    var mod = _Module()
    _Module.createStream = mod.createStream
    _Module.convert = mod.convert
    _Module.readFile = mod.readFile
    _Module.request = mod.request
    _Module.Stream = streamClass
    return _Module
  }


  // Export the module --------------------
  if (typeof module !== 'undefined')
    module.exports = StreamModule
  else if (typeof exports !== 'undefined')
    exports = module.exports = StreamModule
  else
    root.StreamModule = StreamModule
}).call(this)
