# aXml

_Xml and Html parser and serializer._

  [![NPM Version](https://badge.fury.io/js/axml.svg)](https://badge.fury.io/js/axml)
  [![Build Status](https://travis-ci.org/AxFab/axml.svg?branch=master)](https://travis-ci.org/AxFab/axml)

The main functionality of this library is the __parsing of Xml__ data and the conversion __to JsonML__ (javascript object that support most of xml requirements.

Tricks are also build-in (or in development) to retrieve other information like doctype, xml declaration or cdata.

The component also have a `XmlNode` builder (based on W3C specification) and some tolerance again xml standard that allow to parse and manipulate HTML DOM.

### Install
```bash
npm install npm
```

### Usage
```js
var fs = require('fs')
    axml = require('axml')

var fStm = fs.createReadStream('My file')
var xStm = axml.createStream()
fStm.pipe(xStm);
xStm.once ('finish', function (err, data) {
  if (err) throw err;

  // We got JSONML object here
  console.log (JSON.stringfy(data, null, 2));

  // If we want a W3C XmlDocument
  var document = axml.createDocument (data)
  var whatImLookingFor = document.getElementById('my-book').nextSibling
})

```

## JsonML

JsonML, the JSON Markup Language is a lightweight markup language used to map between XML (Extensible Markup Language) and JSON (JavaScript Object Notation)[^wikipedia].

JsonML allows any XML document to be represented uniquely as a JSON string. The syntax uses:

- JSON arrays to represent XML elements;
-  JSON objects to represent attributes;
-  JSON strings to represent text nodes.

XML
```xml
<person created="2006-11-11T19:23" modified="2006-12-31T23:59">
    <firstName>Robert</firstName>
    <lastName>Smith</lastName>
    <address type="home">
        <street>12345 Sixth Ave</street>
        <city>Anytown</city>
        <state>CA</state>
        <postalCode>98765-4321</postalCode>
    </address>
</person>
```

JsonML
```js
["person",
  {"created":"2006-11-11T19:23",
   "modified":"2006-12-31T23:59"},
  ["firstName", "Robert"],
  ["lastName", "Smith"],
  ["address", {"type":"home"},
    ["street", "12345 Sixth Ave"],
    ["city", "Anytown"],
    ["state", "CA"],
    ["postalCode", "98765-4321"]
  ]
]
```

## W3C XmlNode

An XmlNode is an object that recrete the DOM model defined as in W3C specification.
However my version is far from complete and most DOM benchmarks are based on rendering, that I don't provide.

However DOM can be browse easily and simple browser-side js-scripts should work.

More on: [W3C School webiste...][3]


## API

### axml ([opt])

Return a new `XmlParser`, this object can be used exactly as the module object `axml`. The optional parameter of this function must be an object that contains any number of this fields:

- `mode`, string value that determine the level of conformance require. value are  (`html`, `xml`, `strict`)
- `serverName`, used for HTTP methods only (see [Jaks, Web module][1], or `axml.request`)

### axml.createStream ([opt])
Create a stream object, similar to NodeJs other streams, the instance come with the methods: write(), end(), on() and once(). This allow to use the function pipe() of a read stream (from fs or http module).

The optional parameter of this function must be an object that contains any number of this fields:

- `mode`, string value that determine the level of conformance require. value are  (`html`, `xml`, `strict`)


### axml.convertData (data)

Read a buffer or a string of valid xml and convert as JSONML object.

### axml.readFile (data [, callback]) or axml.readFileAsync (data)

Allow to read a Xml file and get as response a JsonML object.


### axml.request (req, res, [next])

Respond to a HTTP request. The format of the function made it available for `http.listen` function or for `express` framework. The response send to the client is a valid Json file formed with the content of an xml.

## NPM Statistics

 [![NPM](https://nodei.co/npm/axml.png?downloads=true)](https://nodei.co/npm/axml/)



 [^wikipedia]: the definition of JsonMl is base on the [Wikipedia article][2]


 [1]: http://axfab.net/library.html?nm=jaks&pg=modules#net
 [2]: http://en.wikipedia.org/wiki/JsonML
 [3]:http://www.w3schools.com/dom/dom_nodetype.asp
