

var assert = require('assert'),
    fs = require('fs'),
    axml = require('../lib/XmlParser.js')

var ModuleAPIStream = function (lib) {
  var module = require('../lib/' + lib + '.js')
  test ('Test ' + lib + ' module API - Stream Module', function() {
    assert.equal(typeof module , 'function')
    assert.equal(typeof module.Stream , 'function')
    assert.equal(typeof module.createStream , 'function')
    assert.equal(typeof module.convert , 'function')
    assert.equal(typeof module.readFile , 'function')
    assert.equal(typeof module.request , 'function')
  })

  test ('Test ' + lib + ' module API - Module customization', function() {
    var modp = module()
    assert.equal(typeof modp.createStream , 'function')
    assert.equal(typeof modp.convert , 'function')
    assert.equal(typeof modp.readFile , 'function')
    assert.equal(typeof modp.request , 'function')
  })

  test ('Test ' + lib + ' module API - Stream Object', function() {
    var stream = new module.Stream()
    assert.equal(typeof stream, 'object')
    assert.equal(typeof stream.mimetype, 'string')
    assert.equal(typeof stream.write, 'function')
    assert.equal(typeof stream.end, 'function')
    assert.equal(typeof stream.on, 'function')
    assert.equal(typeof stream.once, 'function')
    // assert.equal(typeof stream.result, 'string')
  })
}

var testStaticConversionJsObject = function (module, input, expected) {
  var item = module.convert(input)
  var result = JSON.stringify(item)
  assert.equal(expected, result)

}

suite('Module API', function () {
  ModuleAPIStream ('XmlParser')
})

suite('XmlParser', function () {
    test ('Xml static convertion', function () {

      testStaticConversionJsObject (axml,
        '<html><head></head><body></body',
        '["html",["head"],["body"]]' )

      testStaticConversionJsObject (axml,
        '<!Doctype xhtml><html>\n<head><!-- I forgot the title -->   </head><body>\t\r\n  \t </body',
        '["html",["head"],["body"]]' )

      testStaticConversionJsObject (axml,
        '<document><body lang="en"><par>Bonjour, c\'est bien <b>Fab</b> ton nom ?</par></body></document>',
        '["document",["body",{"lang":"en"},["par","Bonjour, c\'est bien ",["b","Fab"]," ton nom ?"]]]' )

    })
})

suite ('Parsing', function () {

  test ('Basic', function () {

    fs.writeFileSync ('test.xml', '<D life="42"><L>Yes</L>No</D>' )
    var fStm = fs.createReadStream('test.xml')
    var xStm = axml.createStream()
    fStm.pipe(xStm);
    xStm.once ('finish', function (err, data) {
      if (err) throw err;

      assert.equal (data[0], "D")
      assert.equal (data[1].life, 42)
      assert.equal (data[2][0], "L")
      assert.equal (data[2][1], "Yes")
      assert.equal (data[3], "No")
      assert.equal (typeof data[4], 'undefined')
    })


  })
})

